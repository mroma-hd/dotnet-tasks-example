
public static class TaskExtensions
{
    public static async Task WhenAllThrottled(this IEnumerable<Func<Task>> tasks, int limit)
    {
        // init semaphore
        var semaphore = new SemaphoreSlim(initialCount: limit);

        // build the new list of tasks to run
        var newTasks = new List<Task>();
        foreach(var t in tasks)
        {
            // wait 
            await semaphore.WaitAsync();
            
            // add the task
            newTasks.Add(
                Task.Run(async () =>
                {
                    try
                    {
                        await t();
                    }
                    finally
                    {
                        // release
                        semaphore.Release();
                    }
                }));
        }
        
        // run all
        await Task.WhenAll(newTasks);    
    }
}
