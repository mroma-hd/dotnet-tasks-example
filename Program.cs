﻿// See https://aka.ms/new-console-template for more information

const int max = 5;

// example task to run
async Task RunTask(int i)
{
    // simulate a wait
    Console.WriteLine($"Running {i}");
    await Task.Delay(1000);    
}

// run serial
async Task RunSerial()
{
    // start    
    var startTime = DateTime.Now;
    Console.WriteLine("\nRunning serial");

    // run in a lopp
    for(int i =1; i <= max; i++)
    {
        await RunTask(i);
    }

    // end
    var endTime = DateTime.Now;
    Console.WriteLine($"Done: {startTime} to {endTime}: {-(startTime-endTime).TotalSeconds} second(s)");    
}

// run async
async Task RunAsync()
{
    // start
    var startTime = DateTime.Now;
    Console.WriteLine("\nRunning async");

    // build tasks to run
    var tasks = Enumerable.Range(1, max).Select(i => RunTask(i));

    // run all
    await Task.WhenAll(tasks);    

    // end
    var endTime = DateTime.Now;
    Console.WriteLine($"Done: {startTime} to {endTime}: {-(startTime-endTime).TotalSeconds} second(s)");   
}

// run async with throttle
async Task RunAsyncThrottled()
{

    // start
    var startTime = DateTime.Now;
    Console.WriteLine("\nRunning async (throttled 3)");
    
    // build tasks to run
    var tasks = new List<Func<Task>>();
    foreach(var i in Enumerable.Range(1, max))
    {        
        tasks.Add(async() => 
        {
            await RunTask(i);
        });
    }

    // run all    
    await tasks.WhenAllThrottled(3);

    // end
    var endTime = DateTime.Now;
    Console.WriteLine($"Done: {startTime} to {endTime}: {-(startTime-endTime).TotalSeconds} second(s)");       
}


Console.WriteLine("Apps Task Example");
await RunSerial();
await RunAsync();
await RunAsyncThrottled();

